/**
 * Created by Albert on 15/07/2014.
 */
'use strict';

angular.module('msContentItemNavigation')
  .directive('msContentItemNavigation', ['PageRepository', function (PageRepository) {
    return {
      restrict: 'A',
      replace: true,
      template: '<nav class="ms-block navigation-block"><a ng-href="/#{{page.url}}" ng-repeat="page in pages">{{page.name}}</a></nav>',
      link: function (scope) {
        PageRepository.getPages().then(function (pages) {
          scope.pages = pages;
        });
      }
    };
  }]);